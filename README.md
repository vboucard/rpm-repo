# rpm-repo

## Objectifs

Créer un dépôt RPM de logiciels comppilés depuis leur source.

Les logiciels compilés seront :
- httpd
- openssl

## httpd
### Depuis la source fournie par Apache
[Gist](https://gist.github.com/rizkhanriaz/9e7d581eda271ecf4fba7dd2636fec9f)

```
wget https://dlcdn.apache.org/apr/apr-1.7.2.tar.bz2
wget https://dlcdn.apache.org/apr/apr-util-1.6.3.tar.bz2
wget https://dlcdn.apache.org/httpd/httpd-2.4.55.tar.bz2

rpmbuild -tb apr-util-1.6.3.tar.bz2
rpmbuild -tp apr-1.7.2.tar.bz2 # --nocheck

cd rpmbuild/RPMS/x86_64/
sudo rpm -U apr-1.7.2-1.x86_64.rpm apr-devel-1.7.2-1.x86_64.rpm 
sudo rpm -U apr-util-1.6.3-1.x86_64.rpm apr-util-devel-1.6.3-1.x86_64.rpm 
```
[https://blog-k--res-net.translate.goog/archives/2384.html?_x_tr_sl=auto&_x_tr_tl=fr&_x_tr_hl=fr](https://blog-k--res-net.translate.goog/archives/2384.html?_x_tr_sl=auto&_x_tr_tl=fr&_x_tr_hl=fr)

Décompresser l'archive httpd et modifier le fichier spec :
```
tar xvjf httpd-2.4.55.tar.bz2
vi httpd-2.4.55.tar.bz2/httpd.spec
```
Ajouter `--enable-http2` dans la section configure et `%{_libdir}/httpd/modules/mod_http2.so` plus bas.

```
mv httpd-2.4.55.tar.bz2 rpmbuild/SOURCES
cd httpd-2.4.55
rpmbuild -bb httpd.spec
```

Problème rencontré : le serveur ainsi packagé n'est pas reconnu par systemd, je vais tenter une autre approche : récupérer le fichier spec du git de centos et compiler le serveur avec ce fichier.
### Compiler depuis le Source RPM (version 2.4.6)
```
cd
wget  http://vault.centos.org/7.9.2009/os/Source/SPackages/httpd-2.4.6-95.el7.centos.src.rpm
mkdir httpd
cd httpd
rpm2cpio ../httpd-2.4.6-95.el7.centos.src.rpm |cpio -iv
cp * ../rpmbuild/SOURCES/
rpmbuild -bb httpd.spec
```
OK, ça compile bien
### Essayer de compiler depuis le git de centos (version 2.4.6)
```
cd
git clone https://git.centos.org/rpms/httpd.git
cd httpd
git checkout c7
cp SOURCES/* ../rpmbuild/SOURCES/
wget -O ../rpmbuild/SOURCES/apache-poweredby.png https://www.apache.org/foundation/press/kit/poweredBy/pb-apache.PNG
wget -P ../rpmbuild/SOURCES/ https://archive.apache.org/dist/httpd/httpd-2.4.6.tar.bz2
```

```
wget http://mirror.centos.org/centos/7/os/x86_64/Packages/httpd-2.4.6-95.el7.centos.x86_64.rpm
```

J'ai essayé de compiler sur la machine compile2 (almalinux 9), le fichier spec de la branche c9
du [dépôt git de almalinux](https://git.almalinux.org/rpms/httpd.git), aucun problème rencontré.
### Dépôt IUS (version 2.4.55)
Le dépôt [IUS](https://repo.ius.io/) semble avoir le paquet httpd à jour. Je télécharge le RPM source pour voir.
```
cd
rm -fr httpd
wget https://repo.ius.io/7/src/packages/h/httpd24u-2.4.55-1.el7.ius.src.rpm
mkdir httpd && cd httpd
rpm2cpio ../httpd24u-2.4.55-1.el7.ius.src.rpm |cpio -iv
sha256sum httpd-2.4.55.tar.bz2
```
`11d6ba19e36c0b93ca62e47e6ffc2d2f2884942694bce0f23f39c71bdc5f69ac` qui est la même somme de contrôle que [le site officiel](https://downloads.apache.org/httpd/httpd-2.4.55.tar.bz2.sha256).
## Ressources

[https://www.golinuxcloud.com/how-to-create-rpm-package-in-linux/](https://www.golinuxcloud.com/how-to-create-rpm-package-in-linux/)