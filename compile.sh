yum update -y

yum install autoconf expat-devel libtool libnghttp2-devel pcre-devel -y
yum install -y curl jq openssl-devel rpm-build tree doxygen git

# pour builder httpd
yum install -y libuuid-devel apr-devel apr-util-devel openldap-devel lua-devel libxml2-devel
yum install -y perl-generators xmlto brotli-devel systemd-devel
# pour builder apr-util
# yum install -y db4-devel postgresql-devel mysql-devel sqlite-devel unixODBC-devel nss-devel